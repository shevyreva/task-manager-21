package ru.t1.shevyreva.tm.service;

import ru.t1.shevyreva.tm.api.repository.IRepository;
import ru.t1.shevyreva.tm.api.service.IService;
import ru.t1.shevyreva.tm.enumerated.Sort;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.IndexEmptyException;
import ru.t1.shevyreva.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(R repository) {
        this.repository = repository;
    }

    public M add(final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    public void clear() {
        repository.clear();
    }

    public M remove(final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.remove(model);
    }

    public List<M> findAll() {
        return repository.findAll();
    }

    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findOneById(id);
        return model;
    }

    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize() <= index) throw new IndexEmptyException();
        final M model = repository.findOneByIndex(index);
        return model;
    }

    public Integer getSize() {
        return repository.getSize();
    }

    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    public void removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize() <= index) throw new IndexEmptyException();
        repository.removeByIndex(index);
    }

    public boolean existsById(final String id) {
        return repository.existsById(id);
    }

}
