package ru.t1.shevyreva.tm.api.service;

import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    void bindTaskToProject(String userId, String projectId, String taskId);

    void unbindTaskToProject(String userId, String projectId, String taskId);

    void removeByProjectId(String userId, String projectId);

    List<Task> findAllTaskByProjectId(String userId, String projectId);

}
