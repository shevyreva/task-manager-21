package ru.t1.shevyreva.tm.api.repository;

import ru.t1.shevyreva.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(final M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    void clear();

    M findOneById(String Id);

    M findOneByIndex(Integer index);

    M remove(M model);

    M removeById(String Id);

    void removeByIndex(Integer index);

    boolean existsById(String id);

    Integer getSize();

}
