package ru.t1.shevyreva.tm.api.repository;

import ru.t1.shevyreva.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

}
