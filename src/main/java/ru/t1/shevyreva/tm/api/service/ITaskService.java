package ru.t1.shevyreva.tm.api.service;

import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.enumerated.Sort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task, ITaskRepository> {

    Task create(String userId, String name, String description);

    Task create(String userId, String name);

    Task updateByIndex(String userId, Integer index, String name, String description);

    Task updateById(String userId, String id, String name, String description);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

    Task changeTaskStatusById(String userId, String id, Status status);

    List<Task> findAll(String userId, Sort sort);

    List<Task> findAllByProjectId(String userId, String projectId);

}
