package ru.t1.shevyreva.tm.exception.user;

public class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Error! You are not logged in. Please, log in and try again.");
    }

}
