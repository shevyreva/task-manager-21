package ru.t1.shevyreva.tm.command;

import ru.t1.shevyreva.tm.api.model.ICommand;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public String getUserId() {
        return serviceLocator.getAuthService().getUserId();
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract String getArgument();

    public abstract void execute();

    public abstract Role[] getRoles();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (description != null && !description.isEmpty()) displayName += ": " + description;
        return displayName;
    }

}
