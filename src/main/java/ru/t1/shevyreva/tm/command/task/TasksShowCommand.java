package ru.t1.shevyreva.tm.command.task;

import ru.t1.shevyreva.tm.enumerated.Sort;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TasksShowCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Show all tasks.";

    private final String NAME = "task-list";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAll(userId, sort);
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

}
