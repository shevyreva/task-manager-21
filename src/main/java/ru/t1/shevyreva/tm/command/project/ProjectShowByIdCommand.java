package ru.t1.shevyreva.tm.command.project;

import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    private final String NAME = "project-show-by-id";

    private final String DESCRIPTION = "Show project by Id.";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Project project = getProjectService().findOneById(userId, id);
        showProject(project);
    }

}
