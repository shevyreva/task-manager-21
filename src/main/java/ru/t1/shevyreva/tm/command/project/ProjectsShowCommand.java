package ru.t1.shevyreva.tm.command.project;

import ru.t1.shevyreva.tm.enumerated.Sort;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectsShowCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Show all projects.";

    private final String NAME = "project-list";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects = getProjectService().findAll(userId, sort);
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

}
