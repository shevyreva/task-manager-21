package ru.t1.shevyreva.tm.command.user;

import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class UserRemoveByLogin extends AbstractUserCommand {

    private final String DESCRIPTION = "User remove by login.";

    private final String NAME = "user-remove-by-login";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER BY LOGIN]:");
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);

    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
