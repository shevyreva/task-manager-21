package ru.t1.shevyreva.tm.command.user;

import ru.t1.shevyreva.tm.enumerated.Role;

public class UserLogoutCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "User logout.";

    private final String NAME = "logout";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        getAuthService().logout();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
