package ru.t1.shevyreva.tm.command.system;

import ru.t1.shevyreva.tm.command.AbstractCommand;

import java.util.Collection;

public class ListCommand extends AbstractSystemCommand {

    private final String DESCRIPTION = "Show command list.";

    private final String NAME = "commands";

    private final String ARGUMENT = "-cmd";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
